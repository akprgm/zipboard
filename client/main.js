var Users=new Mongo.Collection('users');
var Posts=new Mongo.Collection('posts');
//routes
Router.route('/', function () {//root route
    if(window.sessionStorage.getItem('id'))
        window.location.href="/dashboard";
    else  
        this.render('login');
});
Router.route('/login', function () {//route for login template
    if(window.sessionStorage.getItem('id'))
        window.location.href="/dashboard";
    else
        this.render('login');
}); 
Router.route('/register', function () {//route for register template
    if(window.sessionStorage.getItem('id'))
        window.location.href="/dashboard";
    else
        this.render('register');
});
Router.route('/dashboard',function(){//route for dashboard template
    this.render('dashboard');
});


//Event Handlers..................................
Template.login.events({//for login form
    'submit #login': function(e){
        e.preventDefault();
        var emailId=$('#userEmail').val();
        var password=$('#userPwd').val();
        var result=Users.find({email:emailId});
        if(result.count()){//emailId 
            var data=result.fetch()[0];
            if(password===data.pwd){
                window.sessionStorage.setItem('id',data._id);
                window.location.href="/dashboard";    
            }else{
                $('#errorMsg').text("Invalid Email Id or Password");
                $('#errorMsg').addClass('show errorMsgRed');
                $('#errorMsg').removeClass('hide');
                document.getElementById('login').reset(); 
            }
            
        }else{
            $('#errorMsg').text("Email Id not registered");
            $('#errorMsg').addClass('show errorMsgRed');
            $('#errorMsg').removeClass('hide');
            document.getElementById('login').reset();  
            
        }
        
    }
});


Template.register.events({//for register form
    'submit #register': function(e){
        e.preventDefault();
        var emailId=$('#userEmail').val();
        var password=$('#userPwd').val();
        var data={
            email:emailId,
            pwd:password
        }
        var result=Users.find({email:emailId});
        if(!result.count()){//if email id not found create new user
            Users.insert(data);
            $('#errorMsg').text("User Registered Successfully");
            $('#errorMsg').addClass('show errorMsgGreen');
            $('#errorMsg').removeClass('hide');
            document.getElementById('register').reset();
        }
        else{//email id already registered
            $('#errorMsg').text("User already Exists");
            $('#errorMsg').addClass('show errorMsgRed');
            $('#errorMsg').removeClass('hide');
            document.getElementById('register').reset();   
        }
    }
});

Template.dashboard.onCreated(function checkLogin() {
        if(!window.sessionStorage.getItem('id'))
            window.location.href="../";
});

Template.dashboard.helpers({
    email:function(){
            var id=window.sessionStorage.getItem('id');
            var email=Users.find({_id:id}).fetch()[0].email;
            return email;
        },
    post:function(){
        var id=window.sessionStorage.getItem('id');
        var emailId=Users.find({_id:id}).fetch()[0].email;  
        return Posts.find({email:emailId});
    }
});

Template.dashboard.events({
    'click #logout':function(){
        window.sessionStorage.removeItem('id');
        window.location.href="../";
    },
    'submit #post':function(e){
        e.preventDefault()
        var postMsg=$('#postArea').val();
        console.log(postMsg);
        var id=window.sessionStorage.getItem('id');
         var emailId=Users.find({_id:id}).fetch()[0].email;
        var data={
            email:emailId,
            msg:postMsg,
            time:new Date()
        }
        Posts.insert(data);
        document.getElementById('post').reset();
    }    
});